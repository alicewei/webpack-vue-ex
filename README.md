## 注意事項
 *** index.pug ***

## branch
 - master*
 - addBootstrap

## 使用工具
 - webpack 3
 - yarn 1.6.0
 - node 8.0.0
 - 自行添加資料夾 public/
 - 使用pug
 - vue-loader 必須是 v14.2.2
 - 使用 prerender-spa-plugin 產生靜態頁面優化 SEO

```sh
$ curl xxx.com
```

## 指令
https://localhost:8081

```sh
$ yarn install       // 更新並下載包包們
$ yarn dev           // 預覽
$ yarn prod          // 產生
$ brew upgrade yarn  // 更新yarn她自己
```

## 功能
 - uglify JS
 - css link via JS module setting
 - scss -> autoprefixer -> css
 - pug -> html
 - images/ COPY
 - url scan X