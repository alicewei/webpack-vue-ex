// 載入及轉譯css不需另外工具，不會自動打開瀏覽器，卻能自動更新
import './main.scss';
import Vue from 'vue'
import App from './App.vue';

new Vue({
    el: '#app',
    render: h => h(App),
})
